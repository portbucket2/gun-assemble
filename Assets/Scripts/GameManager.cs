﻿using System;
using System.Collections;
using DG.Tweening;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    public GunAsset gunAsset;
    public Transform asimulatedGunHolder;
    public Material fadeMaterial;
    public float distanceMagnitude = 2f;
    public float snappingDistance = 0.35f;
    public float deassambleTime = 0.3f;

    public GameObject playButton;

    private void Awake()
    {
        instance = this;
        DOTween.Init();
        //PlayerPrefs.DeleteKey("CurrentLevel");
    }

    private void Start()
    {

    }

    public void OnStartGame()
    {
        playButton.SetActive(false);
        LoadCurrentLevel();
    }

    public void OnNextGame()
    {
        Application.LoadLevel(0);
    }

    int currentLevel;
    private void LoadCurrentLevel()
    {
        currentLevel = PlayerPrefs.GetInt("CurrentLevel", 0);

        int maxLevel = 0;
        for (int i = 0; i < gunAsset.gunCollection.Length; i++)
        {
            maxLevel += gunAsset.gunCollection[i].allGuns.Length;
        }

        if (currentLevel >= maxLevel)
        {

            Debug.Log("No more gun. Starting game from First");

            PlayerPrefs.SetInt("CurrentLevel", 0);
            currentLevel = 0;
        }
        PlayerPrefs.SetInt("CurrentLevel", PlayerPrefs.GetInt("CurrentLevel") + 1);

        int index = currentLevel;
        for (int i = 0; i < gunAsset.gunCollection.Length; i++)
        {
            if(index < gunAsset.gunCollection[i].allGuns.Length)
            {
                // Here is exist current level
                currentLodedGun = gunAsset.gunCollection[i].allGuns[index];

            }
            else
            {

                index -= gunAsset.gunCollection[i].allGuns.Length;
            }
        }


        StartCoroutine (DeAssembleGun());
    }

    Transform currentLodedGun, deassembleGun, assembleGun;
    IEnumerator DeAssembleGun()
    {
        Vector3 pos;

        assembleGun = Instantiate(currentLodedGun, asimulatedGunHolder);
        foreach (Transform gunParts in assembleGun)
        {
            gunParts.GetComponent<MeshRenderer>().material = fadeMaterial;
        }
        assembleGun.GetComponent<MeshRenderer>().material = fadeMaterial;


        deassembleGun = Instantiate(currentLodedGun, asimulatedGunHolder);
        int totalObjectCount = deassembleGun.childCount + 1;
        float angle = 360 / totalObjectCount * 10;
        foreach (Transform gunParts in deassembleGun)
        {
            gunParts.gameObject.AddComponent<DesignDragger>();
            MeshCollider partHolder = gunParts.gameObject.AddComponent<MeshCollider>();
            partHolder.sharedMesh = gunParts.gameObject.GetComponent<MeshFilter>().mesh;
            //pos = new Vector3(gunParts.position.x * angle * Time.deltaTime, gunParts.position.y, gunParts.position.z * angle * Time.deltaTime);
            pos = UnityEngine.Random.insideUnitCircle.normalized;
            pos.z = pos.y;
            pos.y = gunParts.position.y;
            gunParts.DOMove(pos, deassambleTime);
            yield return new WaitForSeconds(deassambleTime);
        }
        int loopCount = 5;
        while (deassembleGun.childCount > 0)
        {
            foreach (Transform gunParts in deassembleGun)
            {
                gunParts.parent = asimulatedGunHolder;
            }

            loopCount--;
            if (loopCount <= 0)
                break;
        }

        deassembleGun.gameObject.AddComponent<DesignDragger>();
        MeshCollider lastPartHolder = deassembleGun.gameObject.AddComponent<MeshCollider>();
        lastPartHolder.sharedMesh = deassembleGun.gameObject.GetComponent<MeshFilter>().mesh;
        //pos = new Vector3(deassembleGun.position.x * angle, deassembleGun.position.y, deassembleGun.position.z * angle);
        pos = UnityEngine.Random.insideUnitCircle.normalized;
        pos.z = pos.y;
        pos.y = deassembleGun.position.y;
        deassembleGun.DOMove(pos, deassambleTime);

        
    }
}
