﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Painter : MonoBehaviour
{
    public MeshFilter meshFilter;
    public Color color;
    public Vector3 paintOffest;
    public Transform sprayBottle;
    public LineRenderer line;
    public Material lineMaterial;

    Mesh mesh;
    int[] triangles;
    Color[] c;
    private void Awake()
    {
        mesh = meshFilter.mesh;
        triangles = mesh.triangles;
        c = new Color[meshFilter.mesh.vertices.Length];
        for (int i = 0; i < c.Length; i++)
        {
            c[i] = Color.white;
        }
        meshFilter.mesh.colors = c;
        lineMaterial.SetColor("_Color", color);
    }

    void Update()
    {

        if (Input.GetMouseButton(0) && mesh != null)
        {

            //meshFilter.transform.eulerAngles += new Vector3(0f, -1f, 0);

            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition + paintOffest);

            if (!Physics.Raycast(ray, out hit))
                return;

            line.SetPosition(0, sprayBottle.position);
            line.SetPosition(1, hit.point);
            Vector3 bottlePos = GetWorldPositionOnPlane( Input.mousePosition, sprayBottle.position.z);
            bottlePos.z = sprayBottle.position.z;
            sprayBottle.position = bottlePos;

            c[triangles[hit.triangleIndex * 3 + 0]] = color;
            if (hit.triangleIndex * 3 - 1 >= 0)
                c[triangles[hit.triangleIndex * 3 + 1]] = color;
            if (hit.triangleIndex * 3 + 1 < triangles.Length)
                c[triangles[hit.triangleIndex * 3 + 1]] = color;

            mesh.colors = c;
            //mesh.RecalculateNormals();
            //mesh.RecalculateTangents();
        }
    }

    public Vector3 GetWorldPositionOnPlane(Vector3 screenPosition, float z)
    {
        Ray ray = Camera.main.ScreenPointToRay(screenPosition);
        Plane xy = new Plane(-Vector3.forward, new Vector3(0, 0, z));
        float distance;
        xy.Raycast(ray, out distance);
        return ray.GetPoint(distance);
    }
}
