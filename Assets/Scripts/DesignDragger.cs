﻿using UnityEngine;

public class DesignDragger : MonoBehaviour
{

    Vector3 initialPos;
    bool isPlaced = false;
    private void Awake()
    {
        initialPos = transform.position;
    }

    private void OnMouseDrag()
    {
        if (!isPlaced)
        {

            Vector3 pos = GetWorldPositionOnPlane(Input.mousePosition, 0);
            //pos.z = pos.y;
            pos.y = transform.position.y;
            transform.position = pos;

            if (Vector3.Distance(transform.position, initialPos) <= GameManager.instance.snappingDistance)
            {
                isPlaced = true;
                transform.position = initialPos;
            }
        }
    }

    private void Update()
    {
        if (Input.GetMouseButtonUp(0))
            isPlaced = false;
    }


    public Vector3 GetWorldPositionOnPlane(Vector3 screenPosition, float z)
    {
        Ray ray = Camera.main.ScreenPointToRay(screenPosition);
        Plane xy = new Plane(Camera.main.transform.forward, new Vector3(0, 0, z));
        float distance;
        xy.Raycast(ray, out distance);
        return ray.GetPoint(distance);
    }
}