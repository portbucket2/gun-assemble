﻿using UnityEngine;

[CreateAssetMenu(fileName ="GunAsset", menuName ="Gun/GunAsset")]
public class GunAsset : ScriptableObject
{
    public GunTypes[] gunCollection;
}

[System.Serializable]
public class GunTypes
{
    public string gunTypeName;
    public Transform[] allGuns;
}
