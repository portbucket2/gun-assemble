<Q                         DIRLIGHTMAP_COMBINED   _ADDITIONAL_LIGHTS     _MAIN_LIGHT_SHADOWS    _SHADOWS_SOFT       �(  ���(      4                          xlatMtlMain #include <metal_stdlib>
#include <metal_texture>
using namespace metal;

#if !(__HAVE_FMA__)
#define fma(a,b,c) ((a) * (b) + (c))
#endif

constant float4 ImmCB_0[4] =
{
	float4(1.0, 0.0, 0.0, 0.0),
	float4(0.0, 1.0, 0.0, 0.0),
	float4(0.0, 0.0, 1.0, 0.0),
	float4(0.0, 0.0, 0.0, 1.0)
};
#ifndef XLT_REMAP_O
	#define XLT_REMAP_O {0, 1, 2, 3, 4, 5, 6, 7}
#endif
constexpr constant uint xlt_remap_o[] = XLT_REMAP_O;
struct FGlobals_Type
{
    float4 _MainLightPosition;
    half4 _MainLightColor;
    half4 _AdditionalLightsCount;
    float4 _AdditionalLightsPosition[32];
    half4 _AdditionalLightsColor[32];
    half4 _AdditionalLightsAttenuation[32];
    half4 _AdditionalLightsSpotDir[32];
    float4 hlslcc_mtx4x4_MainLightWorldToShadow[20];
    half4 _MainLightShadowOffset0;
    half4 _MainLightShadowOffset1;
    half4 _MainLightShadowOffset2;
    half4 _MainLightShadowOffset3;
    half4 _MainLightShadowParams;
};

struct UnityPerDraw_Type
{
    float4 hlslcc_mtx4x4unity_ObjectToWorld[4];
    float4 hlslcc_mtx4x4unity_WorldToObject[4];
    float4 unity_LODFade;
    half4 unity_WorldTransformParams;
    half4 unity_LightData;
    half4 unity_LightIndices[2];
    float4 unity_ProbesOcclusion;
    half4 unity_SpecCube0_HDR;
    float4 unity_LightmapST;
    float4 unity_DynamicLightmapST;
    half4 unity_SHAr;
    half4 unity_SHAg;
    half4 unity_SHAb;
    half4 unity_SHBr;
    half4 unity_SHBg;
    half4 unity_SHBb;
    half4 unity_SHC;
};

struct Mtl_FragmentIn
{
    float3 TEXCOORD0 [[ user(TEXCOORD0) ]] ;
    float3 TEXCOORD1 [[ user(TEXCOORD1) ]] ;
    float3 TEXCOORD3 [[ user(TEXCOORD3) ]] ;
    float3 TEXCOORD5 [[ user(TEXCOORD5) ]] ;
};

struct Mtl_FragmentOut
{
    half4 SV_TARGET0 [[ color(xlt_remap_o[0]) ]];
};

constexpr sampler _mtl_xl_shadow_sampler(address::clamp_to_edge, filter::linear, compare_func::greater_equal);
fragment Mtl_FragmentOut xlatMtlMain(
    constant FGlobals_Type& FGlobals [[ buffer(0) ]],
    constant UnityPerDraw_Type& UnityPerDraw [[ buffer(1) ]],
    sampler samplerunity_SpecCube0 [[ sampler (0) ]],
    sampler sampler_MainLightShadowmapTexture [[ sampler (1) ]],
    texturecube<half, access::sample > unity_SpecCube0 [[ texture(0) ]] ,
    depth2d<float, access::sample > _MainLightShadowmapTexture [[ texture(1) ]] ,
    Mtl_FragmentIn input [[ stage_in ]])
{
    Mtl_FragmentOut output;
    float u_xlat0;
    uint u_xlatu0;
    float4 u_xlat1;
    half4 u_xlat16_1;
    uint u_xlatu1;
    bool u_xlatb1;
    float4 u_xlat2;
    float3 u_xlat3;
    half3 u_xlat16_4;
    half3 u_xlat16_5;
    half3 u_xlat16_6;
    float3 u_xlat7;
    float3 u_xlat8;
    int u_xlati8;
    bool u_xlatb8;
    half3 u_xlat16_11;
    float u_xlat15;
    half u_xlat16_15;
    uint u_xlatu15;
    float u_xlat22;
    half u_xlat16_22;
    float u_xlat23;
    half u_xlat16_25;
    u_xlat0 = dot(input.TEXCOORD3.xyz, input.TEXCOORD3.xyz);
    u_xlat0 = max(u_xlat0, 1.17549435e-38);
    u_xlat0 = rsqrt(u_xlat0);
    u_xlat7.xyz = float3(u_xlat0) * input.TEXCOORD3.xyz;
    u_xlat1.xyz = input.TEXCOORD0.yyy * FGlobals.hlslcc_mtx4x4_MainLightWorldToShadow[1].xyz;
    u_xlat1.xyz = fma(FGlobals.hlslcc_mtx4x4_MainLightWorldToShadow[0].xyz, input.TEXCOORD0.xxx, u_xlat1.xyz);
    u_xlat1.xyz = fma(FGlobals.hlslcc_mtx4x4_MainLightWorldToShadow[2].xyz, input.TEXCOORD0.zzz, u_xlat1.xyz);
    u_xlat1.xyz = u_xlat1.xyz + FGlobals.hlslcc_mtx4x4_MainLightWorldToShadow[3].xyz;
    u_xlat2.xyz = u_xlat1.xyz + float3(FGlobals._MainLightShadowOffset0.xyz);
    u_xlat2.x = float(_MainLightShadowmapTexture.sample_compare(_mtl_xl_shadow_sampler, u_xlat2.xy, saturate(u_xlat2.z), level(0.0)));
    u_xlat3.xyz = u_xlat1.xyz + float3(FGlobals._MainLightShadowOffset1.xyz);
    u_xlat2.y = float(_MainLightShadowmapTexture.sample_compare(_mtl_xl_shadow_sampler, u_xlat3.xy, saturate(u_xlat3.z), level(0.0)));
    u_xlat3.xyz = u_xlat1.xyz + float3(FGlobals._MainLightShadowOffset2.xyz);
    u_xlat2.z = float(_MainLightShadowmapTexture.sample_compare(_mtl_xl_shadow_sampler, u_xlat3.xy, saturate(u_xlat3.z), level(0.0)));
    u_xlat1.xyw = u_xlat1.xyz + float3(FGlobals._MainLightShadowOffset3.xyz);
    u_xlat2.w = float(_MainLightShadowmapTexture.sample_compare(_mtl_xl_shadow_sampler, u_xlat1.xy, saturate(u_xlat1.w), level(0.0)));
    u_xlat16_4.x = half(dot(u_xlat2, float4(0.25, 0.25, 0.25, 0.25)));
    u_xlat16_11.x = (-FGlobals._MainLightShadowParams.x) + half(1.0);
    u_xlat16_4.x = fma(u_xlat16_4.x, FGlobals._MainLightShadowParams.x, u_xlat16_11.x);
    u_xlatb1 = 0.0>=u_xlat1.z;
    u_xlatb8 = u_xlat1.z>=1.0;
    u_xlatb1 = u_xlatb8 || u_xlatb1;
    u_xlat16_4.x = (u_xlatb1) ? half(1.0) : u_xlat16_4.x;
    u_xlat16_11.x = dot((-u_xlat7.xyz), input.TEXCOORD1.xyz);
    u_xlat16_11.x = u_xlat16_11.x + u_xlat16_11.x;
    u_xlat16_11.xyz = half3(fma(input.TEXCOORD1.xyz, (-float3(u_xlat16_11.xxx)), (-u_xlat7.xyz)));
    u_xlat16_5.x = dot(input.TEXCOORD1.xyz, u_xlat7.xyz);
    u_xlat16_5.x = clamp(u_xlat16_5.x, 0.0h, 1.0h);
    u_xlat16_5.x = (-u_xlat16_5.x) + half(1.0);
    u_xlat16_5.x = u_xlat16_5.x * u_xlat16_5.x;
    u_xlat16_5.x = u_xlat16_5.x * u_xlat16_5.x;
    u_xlat16_1 = unity_SpecCube0.sample(samplerunity_SpecCube0, float3(u_xlat16_11.xyz), level(4.05000019));
    u_xlat16_11.x = u_xlat16_1.w + half(-1.0);
    u_xlat16_11.x = fma(UnityPerDraw.unity_SpecCube0_HDR.w, u_xlat16_11.x, half(1.0));
    u_xlat16_11.x = max(u_xlat16_11.x, half(0.0));
    u_xlat16_11.x = log2(u_xlat16_11.x);
    u_xlat16_11.x = u_xlat16_11.x * UnityPerDraw.unity_SpecCube0_HDR.y;
    u_xlat16_11.x = exp2(u_xlat16_11.x);
    u_xlat16_11.x = u_xlat16_11.x * UnityPerDraw.unity_SpecCube0_HDR.x;
    u_xlat16_11.xyz = u_xlat16_1.xyz * u_xlat16_11.xxx;
    u_xlat16_1.xyz = u_xlat16_11.xyz * half3(0.941176474, 0.941176474, 0.941176474);
    u_xlat16_11.x = fma(u_xlat16_5.x, half(0.5), half(0.0399999991));
    u_xlat16_1.xyz = u_xlat16_1.xyz * u_xlat16_11.xxx;
    u_xlat1.xyz = fma(input.TEXCOORD5.xyz, float3(0.479999989, 0.479999989, 0.479999989), float3(u_xlat16_1.xyz));
    u_xlat16_4.x = u_xlat16_4.x * UnityPerDraw.unity_LightData.z;
    u_xlat16_11.x = dot(input.TEXCOORD1.xyz, FGlobals._MainLightPosition.xyz);
    u_xlat16_11.x = clamp(u_xlat16_11.x, 0.0h, 1.0h);
    u_xlat16_4.x = u_xlat16_11.x * u_xlat16_4.x;
    u_xlat16_4.xyz = u_xlat16_4.xxx * FGlobals._MainLightColor.xyz;
    u_xlat2.xyz = fma(input.TEXCOORD3.xyz, float3(u_xlat0), FGlobals._MainLightPosition.xyz);
    u_xlat0 = dot(u_xlat2.xyz, u_xlat2.xyz);
    u_xlat0 = max(u_xlat0, 1.17549435e-38);
    u_xlat0 = rsqrt(u_xlat0);
    u_xlat2.xyz = float3(u_xlat0) * u_xlat2.xyz;
    u_xlat0 = dot(input.TEXCOORD1.xyz, u_xlat2.xyz);
    u_xlat0 = clamp(u_xlat0, 0.0f, 1.0f);
    u_xlat22 = dot(FGlobals._MainLightPosition.xyz, u_xlat2.xyz);
    u_xlat22 = clamp(u_xlat22, 0.0f, 1.0f);
    u_xlat0 = u_xlat0 * u_xlat0;
    u_xlat0 = fma(u_xlat0, -0.9375, 1.00001001);
    u_xlat16_25 = half(u_xlat22 * u_xlat22);
    u_xlat0 = u_xlat0 * u_xlat0;
    u_xlat16_22 = max(u_xlat16_25, half(0.100000001));
    u_xlat0 = u_xlat0 * float(u_xlat16_22);
    u_xlat0 = u_xlat0 * 3.0;
    u_xlat0 = 0.0625 / u_xlat0;
    u_xlat16_25 = half(u_xlat0 + -6.10351562e-05);
    u_xlat16_25 = fma(u_xlat16_25, half(0.0399999991), half(0.479999989));
    u_xlat16_4.xyz = half3(fma(float3(u_xlat16_25), float3(u_xlat16_4.xyz), u_xlat1.xyz));
    u_xlat16_25 = min(FGlobals._AdditionalLightsCount.x, UnityPerDraw.unity_LightData.y);
    u_xlatu0 = uint(int(float(u_xlat16_25)));
    u_xlat16_5.xyz = u_xlat16_4.xyz;
    u_xlatu1 = 0x0u;
    while(true){
        u_xlatb8 = u_xlatu1>=u_xlatu0;
        if(u_xlatb8){break;}
        u_xlati8 = int(u_xlatu1 & 0x3u);
        u_xlatu15 = u_xlatu1 >> 0x2u;
        u_xlat16_25 = dot(UnityPerDraw.unity_LightIndices[int(u_xlatu15)], half4(ImmCB_0[u_xlati8]));
        u_xlati8 = int(float(u_xlat16_25));
        u_xlat2.xyz = fma((-input.TEXCOORD0.xyz), FGlobals._AdditionalLightsPosition[u_xlati8].www, FGlobals._AdditionalLightsPosition[u_xlati8].xyz);
        u_xlat15 = dot(u_xlat2.xyz, u_xlat2.xyz);
        u_xlat15 = max(u_xlat15, 6.10351562e-05);
        u_xlat22 = rsqrt(u_xlat15);
        u_xlat3.xyz = float3(u_xlat22) * u_xlat2.xyz;
        u_xlat23 = float(1.0) / float(u_xlat15);
        u_xlat15 = fma(u_xlat15, float(FGlobals._AdditionalLightsAttenuation[u_xlati8].x), float(FGlobals._AdditionalLightsAttenuation[u_xlati8].y));
        u_xlat15 = clamp(u_xlat15, 0.0f, 1.0f);
        u_xlat15 = u_xlat15 * u_xlat23;
        u_xlat16_25 = dot(float3(FGlobals._AdditionalLightsSpotDir[u_xlati8].xyz), u_xlat3.xyz);
        u_xlat16_25 = fma(u_xlat16_25, FGlobals._AdditionalLightsAttenuation[u_xlati8].z, FGlobals._AdditionalLightsAttenuation[u_xlati8].w);
        u_xlat16_25 = clamp(u_xlat16_25, 0.0h, 1.0h);
        u_xlat16_25 = u_xlat16_25 * u_xlat16_25;
        u_xlat15 = u_xlat15 * float(u_xlat16_25);
        u_xlat16_25 = dot(input.TEXCOORD1.xyz, u_xlat3.xyz);
        u_xlat16_25 = clamp(u_xlat16_25, 0.0h, 1.0h);
        u_xlat16_25 = half(u_xlat15 * float(u_xlat16_25));
        u_xlat16_6.xyz = half3(u_xlat16_25) * FGlobals._AdditionalLightsColor[u_xlati8].xyz;
        u_xlat8.xyz = fma(u_xlat2.xyz, float3(u_xlat22), u_xlat7.xyz);
        u_xlat2.x = dot(u_xlat8.xyz, u_xlat8.xyz);
        u_xlat2.x = max(u_xlat2.x, 1.17549435e-38);
        u_xlat2.x = rsqrt(u_xlat2.x);
        u_xlat8.xyz = u_xlat8.xyz * u_xlat2.xxx;
        u_xlat2.x = dot(input.TEXCOORD1.xyz, u_xlat8.xyz);
        u_xlat2.x = clamp(u_xlat2.x, 0.0f, 1.0f);
        u_xlat8.x = dot(u_xlat3.xyz, u_xlat8.xyz);
        u_xlat8.x = clamp(u_xlat8.x, 0.0f, 1.0f);
        u_xlat15 = u_xlat2.x * u_xlat2.x;
        u_xlat15 = fma(u_xlat15, -0.9375, 1.00001001);
        u_xlat16_25 = half(u_xlat8.x * u_xlat8.x);
        u_xlat8.x = u_xlat15 * u_xlat15;
        u_xlat16_15 = max(u_xlat16_25, half(0.100000001));
        u_xlat8.x = float(u_xlat16_15) * u_xlat8.x;
        u_xlat8.x = u_xlat8.x * 3.0;
        u_xlat8.x = 0.0625 / u_xlat8.x;
        u_xlat16_25 = half(u_xlat8.x + -6.10351562e-05);
        u_xlat16_25 = fma(u_xlat16_25, half(0.0399999991), half(0.479999989));
        u_xlat16_5.xyz = fma(half3(u_xlat16_25), u_xlat16_6.xyz, u_xlat16_5.xyz);
        u_xlatu1 = u_xlatu1 + 0x1u;
    }
    output.SV_TARGET0.xyz = u_xlat16_5.xyz;
    output.SV_TARGET0.w = half(1.0);
    return output;
}
                                 FGlobals�        _MainLightPosition                           _MainLightColor                        _AdditionalLightsCount                         _AdditionalLightsPosition                            _AdditionalLightsColor                         _AdditionalLightsAttenuation                       _AdditionalLightsSpotDir                       _MainLightShadowOffset0                  `     _MainLightShadowOffset1                  h     _MainLightShadowOffset2                  p     _MainLightShadowOffset3                  x     _MainLightShadowParams                   �     _MainLightWorldToShadow                           UnityPerDraw(        unity_LightData                  �      unity_LightIndices                  �      unity_SpecCube0_HDR                  �             unity_SpecCube0                   _MainLightShadowmapTexture                  FGlobals              UnityPerDraw          