<Q                         DIRLIGHTMAP_COMBINED   INSTANCING_ON      _ADDITIONAL_LIGHTS_VERTEX      _ADDITIONAL_LIGHT_SHADOWS      _MAIN_LIGHT_SHADOWS    _MAIN_LIGHT_SHADOWS_CASCADE     e  ���(      4                          xlatMtlMain #include <metal_stdlib>
#include <metal_texture>
using namespace metal;

#if !(__HAVE_FMA__)
#define fma(a,b,c) ((a) * (b) + (c))
#endif

#ifndef XLT_REMAP_O
	#define XLT_REMAP_O {0, 1, 2, 3, 4, 5, 6, 7}
#endif
constexpr constant uint xlt_remap_o[] = XLT_REMAP_O;
struct FGlobals_Type
{
    float4 _MainLightPosition;
    half4 _MainLightColor;
    float4 hlslcc_mtx4x4_MainLightWorldToShadow[20];
    float4 _CascadeShadowSplitSpheres0;
    float4 _CascadeShadowSplitSpheres1;
    float4 _CascadeShadowSplitSpheres2;
    float4 _CascadeShadowSplitSpheres3;
    float4 _CascadeShadowSplitSphereRadii;
    half4 _MainLightShadowParams;
};

struct UnityPerDraw_Type
{
    float4 hlslcc_mtx4x4unity_ObjectToWorld[4];
    float4 hlslcc_mtx4x4unity_WorldToObject[4];
    float4 unity_LODFade;
    half4 unity_WorldTransformParams;
    half4 unity_LightData;
    half4 unity_LightIndices[2];
    float4 unity_ProbesOcclusion;
    half4 unity_SpecCube0_HDR;
    float4 unity_LightmapST;
    float4 unity_DynamicLightmapST;
    half4 unity_SHAr;
    half4 unity_SHAg;
    half4 unity_SHAb;
    half4 unity_SHBr;
    half4 unity_SHBg;
    half4 unity_SHBb;
    half4 unity_SHC;
};

struct Mtl_FragmentIn
{
    float3 TEXCOORD0 [[ user(TEXCOORD0) ]] ;
    float3 TEXCOORD1 [[ user(TEXCOORD1) ]] ;
    float3 TEXCOORD3 [[ user(TEXCOORD3) ]] ;
    float3 TEXCOORD5 [[ user(TEXCOORD5) ]] ;
    float4 TEXCOORD6 [[ user(TEXCOORD6) ]] ;
};

struct Mtl_FragmentOut
{
    half4 SV_TARGET0 [[ color(xlt_remap_o[0]) ]];
};

constexpr sampler _mtl_xl_shadow_sampler(address::clamp_to_edge, filter::linear, compare_func::greater_equal);
fragment Mtl_FragmentOut xlatMtlMain(
    constant FGlobals_Type& FGlobals [[ buffer(0) ]],
    constant UnityPerDraw_Type& UnityPerDraw [[ buffer(1) ]],
    sampler samplerunity_SpecCube0 [[ sampler (0) ]],
    sampler sampler_MainLightShadowmapTexture [[ sampler (1) ]],
    texturecube<half, access::sample > unity_SpecCube0 [[ texture(0) ]] ,
    depth2d<float, access::sample > _MainLightShadowmapTexture [[ texture(1) ]] ,
    Mtl_FragmentIn input [[ stage_in ]])
{
    Mtl_FragmentOut output;
    float4 u_xlat0;
    half4 u_xlat16_0;
    bool4 u_xlatb0;
    float3 u_xlat1;
    half3 u_xlat16_1;
    int u_xlati1;
    uint u_xlatu1;
    half3 u_xlat16_2;
    float3 u_xlat3;
    half u_xlat16_3;
    bool u_xlatb3;
    half3 u_xlat16_4;
    float3 u_xlat6;
    bool u_xlatb6;
    half u_xlat16_7;
    float u_xlat16;
    bool u_xlatb16;
    half u_xlat16_17;
    u_xlat0.xyz = input.TEXCOORD0.xyz + (-FGlobals._CascadeShadowSplitSpheres0.xyz);
    u_xlat0.x = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat1.xyz = input.TEXCOORD0.xyz + (-FGlobals._CascadeShadowSplitSpheres1.xyz);
    u_xlat0.y = dot(u_xlat1.xyz, u_xlat1.xyz);
    u_xlat1.xyz = input.TEXCOORD0.xyz + (-FGlobals._CascadeShadowSplitSpheres2.xyz);
    u_xlat0.z = dot(u_xlat1.xyz, u_xlat1.xyz);
    u_xlat1.xyz = input.TEXCOORD0.xyz + (-FGlobals._CascadeShadowSplitSpheres3.xyz);
    u_xlat0.w = dot(u_xlat1.xyz, u_xlat1.xyz);
    u_xlatb0 = (u_xlat0<FGlobals._CascadeShadowSplitSphereRadii);
    u_xlat16_2.x = (u_xlatb0.x) ? half(-1.0) : half(-0.0);
    u_xlat16_2.y = (u_xlatb0.y) ? half(-1.0) : half(-0.0);
    u_xlat16_2.z = (u_xlatb0.z) ? half(-1.0) : half(-0.0);
    u_xlat16_0.x = (u_xlatb0.x) ? half(1.0) : half(0.0);
    u_xlat16_0.y = (u_xlatb0.y) ? half(1.0) : half(0.0);
    u_xlat16_0.z = (u_xlatb0.z) ? half(1.0) : half(0.0);
    u_xlat16_0.w = (u_xlatb0.w) ? half(1.0) : half(0.0);
    u_xlat16_2.xyz = u_xlat16_2.xyz + u_xlat16_0.yzw;
    u_xlat16_0.yzw = max(u_xlat16_2.xyz, half3(0.0, 0.0, 0.0));
    u_xlat16_2.x = dot(u_xlat16_0, half4(4.0, 3.0, 2.0, 1.0));
    u_xlat16_2.x = (-u_xlat16_2.x) + half(4.0);
    u_xlatu1 = uint(float(u_xlat16_2.x));
    u_xlati1 = int(u_xlatu1) << 0x2;
    u_xlat6.xyz = input.TEXCOORD0.yyy * FGlobals.hlslcc_mtx4x4_MainLightWorldToShadow[(u_xlati1 + 1)].xyz;
    u_xlat6.xyz = fma(FGlobals.hlslcc_mtx4x4_MainLightWorldToShadow[u_xlati1].xyz, input.TEXCOORD0.xxx, u_xlat6.xyz);
    u_xlat6.xyz = fma(FGlobals.hlslcc_mtx4x4_MainLightWorldToShadow[(u_xlati1 + 2)].xyz, input.TEXCOORD0.zzz, u_xlat6.xyz);
    u_xlat1.xyz = u_xlat6.xyz + FGlobals.hlslcc_mtx4x4_MainLightWorldToShadow[(u_xlati1 + 3)].xyz;
    u_xlatb16 = 0.0>=u_xlat1.z;
    u_xlatb3 = u_xlat1.z>=1.0;
    u_xlat16_1.x = _MainLightShadowmapTexture.sample_compare(_mtl_xl_shadow_sampler, u_xlat1.xy, saturate(u_xlat1.z), level(0.0));
    u_xlatb6 = u_xlatb16 || u_xlatb3;
    u_xlat16_2.x = (-FGlobals._MainLightShadowParams.x) + half(1.0);
    u_xlat16_2.x = fma(u_xlat16_1.x, FGlobals._MainLightShadowParams.x, u_xlat16_2.x);
    u_xlat16_2.x = (u_xlatb6) ? half(1.0) : u_xlat16_2.x;
    u_xlat16_2.x = u_xlat16_2.x * UnityPerDraw.unity_LightData.z;
    u_xlat16_7 = dot(input.TEXCOORD1.xyz, FGlobals._MainLightPosition.xyz);
    u_xlat16_7 = clamp(u_xlat16_7, 0.0h, 1.0h);
    u_xlat16_2.x = u_xlat16_7 * u_xlat16_2.x;
    u_xlat16_2.xyz = u_xlat16_2.xxx * FGlobals._MainLightColor.xyz;
    u_xlat1.x = dot(input.TEXCOORD3.xyz, input.TEXCOORD3.xyz);
    u_xlat1.x = max(u_xlat1.x, 1.17549435e-38);
    u_xlat1.x = rsqrt(u_xlat1.x);
    u_xlat6.xyz = u_xlat1.xxx * input.TEXCOORD3.xyz;
    u_xlat3.xyz = fma(input.TEXCOORD3.xyz, u_xlat1.xxx, FGlobals._MainLightPosition.xyz);
    u_xlat16_17 = dot((-u_xlat6.xyz), input.TEXCOORD1.xyz);
    u_xlat16_17 = u_xlat16_17 + u_xlat16_17;
    u_xlat16_4.xyz = half3(fma(input.TEXCOORD1.xyz, (-float3(u_xlat16_17)), (-u_xlat6.xyz)));
    u_xlat16_17 = dot(input.TEXCOORD1.xyz, u_xlat6.xyz);
    u_xlat16_17 = clamp(u_xlat16_17, 0.0h, 1.0h);
    u_xlat16_17 = (-u_xlat16_17) + half(1.0);
    u_xlat16_17 = u_xlat16_17 * u_xlat16_17;
    u_xlat16_17 = u_xlat16_17 * u_xlat16_17;
    u_xlat16_17 = fma(u_xlat16_17, half(0.5), half(0.0399999991));
    u_xlat16_0 = unity_SpecCube0.sample(samplerunity_SpecCube0, float3(u_xlat16_4.xyz), level(4.05000019));
    u_xlat16_4.x = u_xlat16_0.w + half(-1.0);
    u_xlat16_4.x = fma(UnityPerDraw.unity_SpecCube0_HDR.w, u_xlat16_4.x, half(1.0));
    u_xlat16_4.x = max(u_xlat16_4.x, half(0.0));
    u_xlat16_4.x = log2(u_xlat16_4.x);
    u_xlat16_4.x = u_xlat16_4.x * UnityPerDraw.unity_SpecCube0_HDR.y;
    u_xlat16_4.x = exp2(u_xlat16_4.x);
    u_xlat16_4.x = u_xlat16_4.x * UnityPerDraw.unity_SpecCube0_HDR.x;
    u_xlat16_4.xyz = u_xlat16_0.xyz * u_xlat16_4.xxx;
    u_xlat16_1.xyz = u_xlat16_4.xyz * half3(0.941176474, 0.941176474, 0.941176474);
    u_xlat16_1.xyz = half3(u_xlat16_17) * u_xlat16_1.xyz;
    u_xlat1.xyz = fma(input.TEXCOORD5.xyz, float3(0.479999989, 0.479999989, 0.479999989), float3(u_xlat16_1.xyz));
    u_xlat16 = dot(u_xlat3.xyz, u_xlat3.xyz);
    u_xlat16 = max(u_xlat16, 1.17549435e-38);
    u_xlat16 = rsqrt(u_xlat16);
    u_xlat3.xyz = float3(u_xlat16) * u_xlat3.xyz;
    u_xlat16 = dot(input.TEXCOORD1.xyz, u_xlat3.xyz);
    u_xlat16 = clamp(u_xlat16, 0.0f, 1.0f);
    u_xlat3.x = dot(FGlobals._MainLightPosition.xyz, u_xlat3.xyz);
    u_xlat3.x = clamp(u_xlat3.x, 0.0f, 1.0f);
    u_xlat16_17 = half(u_xlat3.x * u_xlat3.x);
    u_xlat16_3 = max(u_xlat16_17, half(0.100000001));
    u_xlat16 = u_xlat16 * u_xlat16;
    u_xlat16 = fma(u_xlat16, -0.9375, 1.00001001);
    u_xlat16 = u_xlat16 * u_xlat16;
    u_xlat16 = float(u_xlat16_3) * u_xlat16;
    u_xlat16 = u_xlat16 * 3.0;
    u_xlat16 = 0.0625 / u_xlat16;
    u_xlat16_17 = half(u_xlat16 + -6.10351562e-05);
    u_xlat16_17 = fma(u_xlat16_17, half(0.0399999991), half(0.479999989));
    u_xlat16_2.xyz = half3(fma(float3(u_xlat16_17), float3(u_xlat16_2.xyz), u_xlat1.xyz));
    output.SV_TARGET0.xyz = half3(fma(input.TEXCOORD6.yzw, float3(0.479999989, 0.479999989, 0.479999989), float3(u_xlat16_2.xyz)));
    output.SV_TARGET0.w = half(1.0);
    return output;
}
                                 FGlobals�  	      _MainLightPosition                           _MainLightColor                        _CascadeShadowSplitSpheres0                   `     _CascadeShadowSplitSpheres1                   p     _CascadeShadowSplitSpheres2                   �     _CascadeShadowSplitSpheres3                   �     _CascadeShadowSplitSphereRadii                    �     _MainLightShadowParams                   �     _MainLightWorldToShadow                            UnityPerDraw(        unity_LightData                  �      unity_SpecCube0_HDR                  �             unity_SpecCube0                   _MainLightShadowmapTexture                  FGlobals              UnityPerDraw          