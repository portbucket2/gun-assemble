<Q                         DIRLIGHTMAP_COMBINED   INSTANCING_ON      _ADDITIONAL_LIGHTS_VERTEX      _MAIN_LIGHT_SHADOWS    _MIXED_LIGHTING_SUBTRACTIVE    _SHADOWS_SOFT       �  ���(      4                          xlatMtlMain #include <metal_stdlib>
#include <metal_texture>
using namespace metal;

#if !(__HAVE_FMA__)
#define fma(a,b,c) ((a) * (b) + (c))
#endif

#ifndef XLT_REMAP_O
	#define XLT_REMAP_O {0, 1, 2, 3, 4, 5, 6, 7}
#endif
constexpr constant uint xlt_remap_o[] = XLT_REMAP_O;
struct FGlobals_Type
{
    float4 _MainLightPosition;
    half4 _MainLightColor;
    float4 hlslcc_mtx4x4_MainLightWorldToShadow[20];
    half4 _MainLightShadowOffset0;
    half4 _MainLightShadowOffset1;
    half4 _MainLightShadowOffset2;
    half4 _MainLightShadowOffset3;
    half4 _MainLightShadowParams;
};

struct UnityPerDraw_Type
{
    float4 hlslcc_mtx4x4unity_ObjectToWorld[4];
    float4 hlslcc_mtx4x4unity_WorldToObject[4];
    float4 unity_LODFade;
    half4 unity_WorldTransformParams;
    half4 unity_LightData;
    half4 unity_LightIndices[2];
    float4 unity_ProbesOcclusion;
    half4 unity_SpecCube0_HDR;
    float4 unity_LightmapST;
    float4 unity_DynamicLightmapST;
    half4 unity_SHAr;
    half4 unity_SHAg;
    half4 unity_SHAb;
    half4 unity_SHBr;
    half4 unity_SHBg;
    half4 unity_SHBb;
    half4 unity_SHC;
};

struct Mtl_FragmentIn
{
    float3 TEXCOORD0 [[ user(TEXCOORD0) ]] ;
    float3 TEXCOORD1 [[ user(TEXCOORD1) ]] ;
    float3 TEXCOORD3 [[ user(TEXCOORD3) ]] ;
    float3 TEXCOORD5 [[ user(TEXCOORD5) ]] ;
    float4 TEXCOORD6 [[ user(TEXCOORD6) ]] ;
};

struct Mtl_FragmentOut
{
    half4 SV_TARGET0 [[ color(xlt_remap_o[0]) ]];
};

constexpr sampler _mtl_xl_shadow_sampler(address::clamp_to_edge, filter::linear, compare_func::greater_equal);
fragment Mtl_FragmentOut xlatMtlMain(
    constant FGlobals_Type& FGlobals [[ buffer(0) ]],
    constant UnityPerDraw_Type& UnityPerDraw [[ buffer(1) ]],
    sampler samplerunity_SpecCube0 [[ sampler (0) ]],
    sampler sampler_MainLightShadowmapTexture [[ sampler (1) ]],
    texturecube<half, access::sample > unity_SpecCube0 [[ texture(0) ]] ,
    depth2d<float, access::sample > _MainLightShadowmapTexture [[ texture(1) ]] ,
    Mtl_FragmentIn input [[ stage_in ]])
{
    Mtl_FragmentOut output;
    float4 u_xlat0;
    half4 u_xlat16_0;
    bool u_xlatb0;
    float4 u_xlat1;
    half u_xlat16_1;
    float3 u_xlat2;
    half3 u_xlat16_3;
    half3 u_xlat16_4;
    float3 u_xlat5;
    bool u_xlatb5;
    half u_xlat16_8;
    float u_xlat15;
    half u_xlat16_18;
    u_xlat0.xyz = input.TEXCOORD0.yyy * FGlobals.hlslcc_mtx4x4_MainLightWorldToShadow[1].xyz;
    u_xlat0.xyz = fma(FGlobals.hlslcc_mtx4x4_MainLightWorldToShadow[0].xyz, input.TEXCOORD0.xxx, u_xlat0.xyz);
    u_xlat0.xyz = fma(FGlobals.hlslcc_mtx4x4_MainLightWorldToShadow[2].xyz, input.TEXCOORD0.zzz, u_xlat0.xyz);
    u_xlat0.xyz = u_xlat0.xyz + FGlobals.hlslcc_mtx4x4_MainLightWorldToShadow[3].xyz;
    u_xlat1.xyz = u_xlat0.xyz + float3(FGlobals._MainLightShadowOffset0.xyz);
    u_xlat1.x = float(_MainLightShadowmapTexture.sample_compare(_mtl_xl_shadow_sampler, u_xlat1.xy, saturate(u_xlat1.z), level(0.0)));
    u_xlat2.xyz = u_xlat0.xyz + float3(FGlobals._MainLightShadowOffset1.xyz);
    u_xlat1.y = float(_MainLightShadowmapTexture.sample_compare(_mtl_xl_shadow_sampler, u_xlat2.xy, saturate(u_xlat2.z), level(0.0)));
    u_xlat2.xyz = u_xlat0.xyz + float3(FGlobals._MainLightShadowOffset2.xyz);
    u_xlat1.z = float(_MainLightShadowmapTexture.sample_compare(_mtl_xl_shadow_sampler, u_xlat2.xy, saturate(u_xlat2.z), level(0.0)));
    u_xlat0.xyw = u_xlat0.xyz + float3(FGlobals._MainLightShadowOffset3.xyz);
    u_xlat1.w = float(_MainLightShadowmapTexture.sample_compare(_mtl_xl_shadow_sampler, u_xlat0.xy, saturate(u_xlat0.w), level(0.0)));
    u_xlat16_3.x = half(dot(u_xlat1, float4(0.25, 0.25, 0.25, 0.25)));
    u_xlat16_8 = (-FGlobals._MainLightShadowParams.x) + half(1.0);
    u_xlat16_3.x = fma(u_xlat16_3.x, FGlobals._MainLightShadowParams.x, u_xlat16_8);
    u_xlatb0 = 0.0>=u_xlat0.z;
    u_xlatb5 = u_xlat0.z>=1.0;
    u_xlatb0 = u_xlatb5 || u_xlatb0;
    u_xlat16_3.x = (u_xlatb0) ? half(1.0) : u_xlat16_3.x;
    u_xlat0.x = float(UnityPerDraw.unity_LightData.z) * UnityPerDraw.unity_ProbesOcclusion.x;
    u_xlat16_3.x = half(float(u_xlat16_3.x) * u_xlat0.x);
    u_xlat16_8 = dot(input.TEXCOORD1.xyz, FGlobals._MainLightPosition.xyz);
    u_xlat16_8 = clamp(u_xlat16_8, 0.0h, 1.0h);
    u_xlat16_3.x = u_xlat16_8 * u_xlat16_3.x;
    u_xlat16_3.xyz = u_xlat16_3.xxx * FGlobals._MainLightColor.xyz;
    u_xlat0.x = dot(input.TEXCOORD3.xyz, input.TEXCOORD3.xyz);
    u_xlat0.x = max(u_xlat0.x, 1.17549435e-38);
    u_xlat0.x = rsqrt(u_xlat0.x);
    u_xlat5.xyz = u_xlat0.xxx * input.TEXCOORD3.xyz;
    u_xlat1.xyz = fma(input.TEXCOORD3.xyz, u_xlat0.xxx, FGlobals._MainLightPosition.xyz);
    u_xlat16_18 = dot((-u_xlat5.xyz), input.TEXCOORD1.xyz);
    u_xlat16_18 = u_xlat16_18 + u_xlat16_18;
    u_xlat16_4.xyz = half3(fma(input.TEXCOORD1.xyz, (-float3(u_xlat16_18)), (-u_xlat5.xyz)));
    u_xlat16_18 = dot(input.TEXCOORD1.xyz, u_xlat5.xyz);
    u_xlat16_18 = clamp(u_xlat16_18, 0.0h, 1.0h);
    u_xlat16_18 = (-u_xlat16_18) + half(1.0);
    u_xlat16_18 = u_xlat16_18 * u_xlat16_18;
    u_xlat16_18 = u_xlat16_18 * u_xlat16_18;
    u_xlat16_18 = fma(u_xlat16_18, half(0.5), half(0.0399999991));
    u_xlat16_0 = unity_SpecCube0.sample(samplerunity_SpecCube0, float3(u_xlat16_4.xyz), level(4.05000019));
    u_xlat16_4.x = u_xlat16_0.w + half(-1.0);
    u_xlat16_4.x = fma(UnityPerDraw.unity_SpecCube0_HDR.w, u_xlat16_4.x, half(1.0));
    u_xlat16_4.x = max(u_xlat16_4.x, half(0.0));
    u_xlat16_4.x = log2(u_xlat16_4.x);
    u_xlat16_4.x = u_xlat16_4.x * UnityPerDraw.unity_SpecCube0_HDR.y;
    u_xlat16_4.x = exp2(u_xlat16_4.x);
    u_xlat16_4.x = u_xlat16_4.x * UnityPerDraw.unity_SpecCube0_HDR.x;
    u_xlat16_4.xyz = u_xlat16_0.xyz * u_xlat16_4.xxx;
    u_xlat16_0.xyz = u_xlat16_4.xyz * half3(0.941176474, 0.941176474, 0.941176474);
    u_xlat16_0.xyz = half3(u_xlat16_18) * u_xlat16_0.xyz;
    u_xlat0.xyz = fma(input.TEXCOORD5.xyz, float3(0.479999989, 0.479999989, 0.479999989), float3(u_xlat16_0.xyz));
    u_xlat15 = dot(u_xlat1.xyz, u_xlat1.xyz);
    u_xlat15 = max(u_xlat15, 1.17549435e-38);
    u_xlat15 = rsqrt(u_xlat15);
    u_xlat1.xyz = float3(u_xlat15) * u_xlat1.xyz;
    u_xlat15 = dot(input.TEXCOORD1.xyz, u_xlat1.xyz);
    u_xlat15 = clamp(u_xlat15, 0.0f, 1.0f);
    u_xlat1.x = dot(FGlobals._MainLightPosition.xyz, u_xlat1.xyz);
    u_xlat1.x = clamp(u_xlat1.x, 0.0f, 1.0f);
    u_xlat16_18 = half(u_xlat1.x * u_xlat1.x);
    u_xlat16_1 = max(u_xlat16_18, half(0.100000001));
    u_xlat15 = u_xlat15 * u_xlat15;
    u_xlat15 = fma(u_xlat15, -0.9375, 1.00001001);
    u_xlat15 = u_xlat15 * u_xlat15;
    u_xlat15 = float(u_xlat16_1) * u_xlat15;
    u_xlat15 = u_xlat15 * 3.0;
    u_xlat15 = 0.0625 / u_xlat15;
    u_xlat16_18 = half(u_xlat15 + -6.10351562e-05);
    u_xlat16_18 = fma(u_xlat16_18, half(0.0399999991), half(0.479999989));
    u_xlat16_3.xyz = half3(fma(float3(u_xlat16_18), float3(u_xlat16_3.xyz), u_xlat0.xyz));
    output.SV_TARGET0.xyz = half3(fma(input.TEXCOORD6.yzw, float3(0.479999989, 0.479999989, 0.479999989), float3(u_xlat16_3.xyz)));
    output.SV_TARGET0.w = half(1.0);
    return output;
}
                                FGlobals�        _MainLightPosition                           _MainLightColor                        _MainLightShadowOffset0                  `     _MainLightShadowOffset1                  h     _MainLightShadowOffset2                  p     _MainLightShadowOffset3                  x     _MainLightShadowParams                   �     _MainLightWorldToShadow                            UnityPerDraw(        unity_LightData                  �      unity_ProbesOcclusion                     �      unity_SpecCube0_HDR                  �             unity_SpecCube0                   _MainLightShadowmapTexture                  FGlobals              UnityPerDraw          